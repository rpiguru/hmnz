import Tkinter as Tk
from functools import partial
import datetime
from widgets.base import BaseBlock
import tkMessageBox
from widgets.const import US_STATES, NUMBER_DICT, BIRTHDAY_FORMATS, US_CITIES, SPECIAL_CHARACTERS


class Adder(BaseBlock):
    """
    Appender & Prepend
    """

    def __init__(self, master=None, main=None, _type='Append', **kwargs):
        BaseBlock.__init__(self, master=master, main=main, title=_type, **kwargs)
        self.sp_from = None
        self.sp_to = None
        self.custom_num_window = None
        self.string_popup = None
        self.entry_string = None
        self.birthday_format = Tk.StringVar()
        self.special_dlg = None
        self.chk_special = []

    def add_upper_button(self):
        """
        Override this function...
        :return:
        """
        mb = Tk.Menubutton(self.upper_frame, text=" + ", relief="raised", font=("Helvetica", "14"))
        mb.menu = Tk.Menu(mb, tearoff=0)
        mb["menu"] = mb.menu
        mb.menu.add_command(label="Do Nothing", command=partial(self.add_item, 'Do Nothing'))

        m_words = Tk.Menu(mb, tearoff=0)
        mb.menu.add_cascade(label='Words ... ', menu=m_words, underline=0)
        m_words.add_command(label="Custom File...", command=partial(self.open_file_dlg, 'File'))
        m_words.add_command(label="Custom String...", command=partial(self.open_string_popup, 'String'))
        m_words.add_command(label="Nothing", command=partial(self.add_item, 'Words - Nothing'))
        m_words.add_command(label="English Dictionary", command=partial(self.add_item, 'Words - English Dictionary'))
        if True:    # Add 3rd submenus
            m_common = Tk.Menu(m_words, tearoff=0)
            m_words.add_cascade(label='Common Names ... ', menu=m_common, underline=0)
            m_common.add_command(label='Men', command=partial(self.add_item, 'Words - Men'))
            m_common.add_command(label='Women', command=partial(self.add_item, 'Words - Women'))
            m_common.add_command(label='Pets', command=partial(self.add_item, 'Words - Pets'))

            m_other = Tk.Menu(m_words, tearoff=0)
            m_words.add_cascade(label='Other ... ', menu=m_other, underline=0)
            m_other.add_command(label='Slang', command=partial(self.add_item, 'Words - Slang'))
            m_other.add_command(label='Expletives', command=partial(self.add_item, 'Words - Expletives'))

        m_numbers = Tk.Menu(mb, tearoff=0)
        mb.menu.add_cascade(label='Numbers ... ', menu=m_numbers, underline=0)
        m_numbers.add_command(label='User Defined... ', command=self.open_custom_number_dlg)
        for n in NUMBER_DICT.keys():
            m_numbers.add_command(label='{}: {}'.format(n.capitalize(), NUMBER_DICT[n]),
                                  command=partial(
                                      self.add_item, 'Numbers: {} {}'.format(n.capitalize(), NUMBER_DICT[n])))
        m_numbers.add_command(label='Birthdays', command=self.open_birthday_dlg)

        mb.menu.add_command(label="Special Characters", command=self.open_special_dlg)

        for _type in ['Area', 'Zip']:
            m_area_zip = Tk.Menu(mb, tearoff=0)
            mb.menu.add_cascade(label='{} Codes (US) ... '.format(_type), menu=m_area_zip, underline=0)
            for sub_type in ['State', 'City']:
                m_sub = Tk.Menu(m_area_zip, tearoff=0)
                m_area_zip.add_cascade(label='By {} ... '.format(sub_type), menu=m_sub, underline=0)
                target_list = US_STATES if sub_type == 'State' else US_CITIES
                for st in target_list:
                    m_sub.add_command(label=st, command=partial(
                        self.add_item, '{} Codes: {} {}'.format(_type, st, sub_type if sub_type == 'State' else '')))

        mb.pack(side="left", fill="x", padx=10, pady=5)

    def on_add(self, _type, *args):
        val1, val2 = _type.split('_')
        if val1 == 'number':
            if val2 == 'user':
                self.open_custom_number_dlg()
            else:
                self.add_item('Numbers: {}'.format(NUMBER_DICT[val2]))
        elif val1 == 'area':
            self.add_item('Area Codes ({})'.format(val2))
        elif val1 == 'special':
            pass
        elif val1 == 'zip':
            pass

    def open_custom_number_dlg(self):
        """
        Compose Custom Number Select dialog and open
        :return:
        """
        self.custom_num_window = Tk.Toplevel()
        self.custom_num_window.title('{}: Number Selection'.format(self.title))
        self.custom_num_window.resizable(width=False, height=False)
        frame = Tk.Frame(self.custom_num_window)
        lb = Tk.Label(frame, text='Select Numbers to {}'.format(self.title))
        lb.pack(fill='both', side='top')

        sp_box = Tk.Frame(frame)
        lb1 = Tk.Label(sp_box, text='From')
        lb1.pack(side='left', padx=5)
        self.sp_from = Tk.Spinbox(sp_box, width=12, from_=0, to=10000)
        self.sp_from.pack(side='left')
        lb2 = Tk.Label(sp_box, text='To')
        lb2.pack(side='left', padx=(50, 5))
        self.sp_to = Tk.Spinbox(sp_box, width=12, from_=0, to=10000)
        self.sp_to.pack(side='right')
        sp_box.pack(fill='both', side='top', padx=30, pady=20)

        btn_box = Tk.Frame(frame)
        btn_cancel = Tk.Button(btn_box, text='Cancel', command=self.cancel_custom_num_window)
        btn_cancel.pack(side='right', padx=10, pady=20)
        btn_ok = Tk.Button(btn_box, text='Ok', command=self.on_ok_custom_num_window)
        btn_ok.pack(side='left', padx=10, pady=20)
        btn_box.pack()
        frame.pack(fill='both', padx=10, pady=10)
        self.custom_num_window.focus_set()

    def cancel_custom_num_window(self, *args):
        if self.custom_num_window:
            self.custom_num_window.destroy()
            self.custom_num_window = None

    def on_ok_custom_num_window(self, *args):
        """
        Callback when `OK` in Custom Number Window is selected.
        :param args:
        :return:
        """
        val_from = int(self.sp_from.get())
        val_to = int(self.sp_to.get())
        if val_from > val_to:
            tkMessageBox.showerror('Invalid Value', 'Please select correct value')
        else:
            self.add_item('Numbers: {} ~ {}'.format(val_from, val_to))
            self.cancel_custom_num_window()

    def on_btn_add(self, *args):
        pass

    def open_string_popup(self, _type):
        """
        Open Case Popup
        :return:
        """
        self.string_popup = Tk.Toplevel()
        self.string_popup.title('Input Custom String ({})'.format(_type))
        self.string_popup.resizable(width=False, height=False)
        frame = Tk.Frame(self.string_popup)
        lb = Tk.Label(frame, text='Input Custom String - {}'.format(self.title))
        lb.pack(fill='both', side='top')

        box = Tk.Frame(frame)
        lb1 = Tk.Label(box, text='String: ')
        lb1.pack(side='left', padx=5)
        self.entry_string = Tk.Entry(box, width=25)
        self.entry_string.pack(side='left')
        box.pack(fill='both', side='top', padx=30, pady=20)

        btn_box = Tk.Frame(frame)
        btn_cancel = Tk.Button(btn_box, text='Cancel', command=self.cancel_string_popup)
        btn_cancel.pack(side='right', padx=10, pady=20)
        btn_ok = Tk.Button(btn_box, text='Ok', command=partial(self.on_ok_string_popup, _type))
        btn_ok.pack(side='left', padx=10, pady=20)
        btn_box.pack()
        frame.pack(fill='both', padx=10, pady=10)
        self.string_popup.focus_set()

    def cancel_string_popup(self, *args):
        if self.string_popup:
            self.string_popup.destroy()
            self.string_popup = None

    def on_ok_string_popup(self, _type, *args):
        """
        Callback when `OK` in Custom Number Window is selected.
        :param _type:
        :param args:
        :return:
        """
        val = self.entry_string.get()
        if val != '':
            self.add_item('{}: {}'.format(_type, val))
            self.cancel_string_popup()

    def open_birthday_dlg(self):
        self.custom_num_window = Tk.Toplevel()
        self.custom_num_window.title('{}: Birthday Selection'.format(self.title))
        self.custom_num_window.resizable(width=False, height=False)
        frame = Tk.Frame(self.custom_num_window)
        lb = Tk.Label(frame, text='Select Birthdays to {}'.format(self.title))
        lb.pack(fill='both', side='top')

        sp_box = Tk.Frame(frame)
        lb1 = Tk.Label(sp_box, text='From')
        lb1.pack(side='left', padx=5)
        cur_year = datetime.date.today().year
        self.sp_from = Tk.Spinbox(sp_box, width=12, from_=1950, to=cur_year)
        self.sp_from.pack(side='left')
        lb2 = Tk.Label(sp_box, text='To')
        lb2.pack(side='left', padx=(50, 5))
        var = Tk.IntVar()
        var.set(str(cur_year))
        self.sp_to = Tk.Spinbox(sp_box, width=12, from_=1950, to=cur_year, textvariable=var)
        self.sp_to.pack(side='right')
        sp_box.pack(fill='both', side='top', padx=30, pady=20)

        drop_down = Tk.OptionMenu(frame, self.birthday_format, *BIRTHDAY_FORMATS)
        self.birthday_format.set('mmddyy')
        drop_down.pack(side='top')
        btn_box = Tk.Frame(frame)
        btn_cancel = Tk.Button(btn_box, text='Cancel', command=self.cancel_custom_num_window)
        btn_cancel.pack(side='right', padx=10, pady=20)
        btn_ok = Tk.Button(btn_box, text='Ok', command=self.on_ok_birthday_window)
        btn_ok.pack(side='left', padx=10, pady=20)
        btn_box.pack()
        frame.pack(fill='both', padx=10, pady=10)
        self.custom_num_window.focus_set()

    def on_ok_birthday_window(self):
        val_from = int(self.sp_from.get())
        val_to = int(self.sp_to.get())
        if val_from > val_to:
            tkMessageBox.showerror('Invalid Value', 'Please select correct value')
        else:
            self.add_item('Birthday: {} ~ {}, format: {}'.format(val_from, val_to, self.birthday_format.get()))
            self.cancel_custom_num_window()

    def open_special_dlg(self):
        self.special_dlg = Tk.Toplevel()
        self.special_dlg.title('Select Special Characters')
        self.special_dlg.resizable(width=False, height=False)
        frame = Tk.Frame(self.special_dlg)
        lb = Tk.Label(frame, text='Select Special Characters'.format(self.title))
        lb.pack(fill='both', side='top')

        box = Tk.Frame(frame)
        self.chk_special = []
        for val in SPECIAL_CHARACTERS:
            var = Tk.IntVar()
            tmp = Tk.Checkbutton(box, text=val, relief=Tk.FLAT, variable=var)
            self.chk_special.append(var)
            tmp.pack(fill='both', side='top')
        box.pack(fill='both', side='top', padx=30, pady=20)

        btn_box = Tk.Frame(frame)
        btn_cancel = Tk.Button(btn_box, text='Cancel', command=self.cancel_special)
        btn_cancel.pack(side='right', padx=10, pady=20)
        btn_ok = Tk.Button(btn_box, text='Ok', command=self.on_ok_special_dlg)
        btn_ok.pack(side='left', padx=10, pady=20)
        btn_box.pack()
        frame.pack(fill='both', padx=60, pady=10)
        self.special_dlg.focus_set()

    def cancel_special(self, *args):
        if self.special_dlg:
            self.special_dlg.destroy()
            self.special_dlg = None

    def on_ok_special_dlg(self, *args):
        """
        :param args:
        :return:
        """
        checked_vals = [SPECIAL_CHARACTERS[i] for i in range(len(SPECIAL_CHARACTERS)) if self.chk_special[i].get() == 1]
        if len(checked_vals) > 0:
            self.add_item('Special Characters: {}'.format(' '.join(checked_vals)))
        self.cancel_special()
