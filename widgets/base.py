# coding=utf-8
from Tkinter import Frame
import Tkinter as Tk
from abc import abstractmethod
from functools import partial
from tkFileDialog import askopenfile


class BaseBlock(Frame):
    """
    Base class of all blocks including BaseFiles, Mutator, Adder.
    This class has `title` at the left-top corner, and has `+` button after that.
    If you need to use other type of button, just customize `add_upper_button` function.
    """

    def __init__(self, master=None, main=None, title='Title', allow_remove=True, number=1, **kwargs):
        Frame.__init__(self, master=master, **kwargs)
        self.title = title
        self.main = main
        self.items = []
        self.number = number
        self.allow_remove = allow_remove
        self.configure(borderwidth=1, relief=Tk.RAISED)
        self.upper_frame = Frame(self)
        self.upper_frame.configure(borderwidth=1, relief=Tk.RIDGE)
        self.lb_title = Tk.Label(self.upper_frame, text='{}. {}'.format(number, title))
        self.lb_title.pack(fill="both", side="left", padx=10, pady=10)
        self.upper_frame.pack(side="top", fill="x", expand=1)
        self.lower_frame = Frame(self)
        self.lower_frame.pack(side="bottom", fill="both")
        self.add_upper_button()
        if self.allow_remove:
            self.add_remove_button()
        self.pack(side="top", fill="both", padx=2, pady=2)

    def add_upper_button(self):
        btn_add_file = Tk.Button(self.upper_frame, text=' + ')
        btn_add_file.pack(fill="both", side="left", padx=10, pady=5)
        btn_add_file.bind("<ButtonRelease-1>", self.on_btn_add)

    def add_remove_button(self):
        btn_add_file = Tk.Button(self.upper_frame, text=' - ')
        btn_add_file.pack(fill="both", side="right", padx=10, pady=5)
        btn_add_file.bind("<ButtonRelease-1>", partial(self.main.on_remove_block, self))

        btn_add_file = Tk.Button(self.upper_frame, text=' ↑ ')
        btn_add_file.pack(fill="both", side="right", padx=10, pady=5)
        btn_add_file.bind("<ButtonRelease-1>", partial(self.main.move_node, self, 'up'))

        btn_add_file = Tk.Button(self.upper_frame, text=' ↓ ')
        btn_add_file.pack(fill="both", side="right", padx=10, pady=5)
        btn_add_file.bind("<ButtonRelease-1>", partial(self.main.move_node, self, 'down'))

    def update_number(self, num):
        self.number = num
        self.lb_title.configure(text='{}. {}'.format(self.number, self.title))

    @abstractmethod
    def on_btn_add(self, *args):
        """
        Inherit this function to customize the behavior when the top-right button is pressed.
        :param args:
        :return:
        """
        pass

    def add_item(self, item_name, *args):
        """
        Add item to the lower frame with `-` button & title
        :param item_name: title to be shown on the right side
        :param args:
        :return:
        """
        if len(self.items) == 0:
            self.lower_frame = Frame(self)
            self.lower_frame.pack(side="bottom", fill="both")
        item = LabeledFrame(self.lower_frame, title=item_name)
        btn_add_file = Tk.Button(item, text=' - ')
        btn_add_file.pack(fill="both", side="left", padx=10, pady=5)
        btn_add_file.bind("<ButtonRelease-1>", partial(self.remove_item, item))
        lb_base_files = Tk.Label(item, text=item_name)
        lb_base_files.pack(fill="both", side="left", padx=10, pady=10)
        item.pack(side="top", fill="both")
        self.items.append(item)

    def remove_item(self, item, *args):
        """
        Callback when `-` button is pressed
        :param item:
        :param args:
        :return:
        """
        item.destroy()
        self.items.remove(item)
        if len(self.items) == 0:
            self.lower_frame.destroy()

    def get_values(self):
        """
        Get the list of values
        :return:
        :rtype: list
        """
        return [item.get_value() for item in self.items]

    def open_file_dlg(self, prefix='File', *args):
        file_path = askopenfile()
        if file_path:
            self.add_item('{}: {}'.format(prefix, file_path.name))


class LabeledFrame(Frame):
    """
    Tkinter Frame which has `title`
    """

    def __init__(self, master=None, title='', **kwargs):
        Frame.__init__(self, master=master, **kwargs)
        self.title = title

    def get_value(self):
        return self.title
