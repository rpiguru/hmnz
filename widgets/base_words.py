import Tkinter as Tk
from functools import partial
from widgets.base import BaseBlock


class BaseFiles(BaseBlock):
    """
    Block for adding base files.
    """

    def __init__(self, master=None, main=None, **kwargs):
        BaseBlock.__init__(self, master=master, main=main, title='Base Words', number=1, allow_remove=False, **kwargs)
        self.add_item('English Dictionary')

    def add_upper_button(self):
        mb = Tk.Menubutton(self.upper_frame, text=" + ", relief="raised", font=("Helvetica", "14"))
        mb.menu = Tk.Menu(mb, tearoff=0)
        mb["menu"] = mb.menu
        mb.menu.add_command(label="Custom File...", command=self.open_file_dlg)
        mb.menu.add_command(label="Do Nothing", command=partial(self.add_item, 'Do Nothing'))
        mb.menu.add_command(label="English Dictionary", command=partial(self.add_item, 'English Dictionary'))

        m_common = Tk.Menu(mb, tearoff=0)
        mb.menu.add_cascade(label='Common Names ... ', menu=m_common, underline=0)
        m_common.add_command(label='Men', command=partial(self.add_item, 'Common Names: Men'))
        m_common.add_command(label='Women', command=partial(self.add_item, 'Common Names: Women'))
        m_common.add_command(label='Pets', command=partial(self.add_item, 'Common Names: Pets'))

        m_other = Tk.Menu(mb, tearoff=0)
        mb.menu.add_cascade(label='Other ... ', menu=m_other, underline=0)
        m_other.add_command(label='Slang', command=partial(self.add_item, 'Other: Slang'))
        m_other.add_command(label='Expletives', command=partial(self.add_item, 'Other: Expletives'))
        mb.pack(side="left", fill="x", padx=10, pady=5)

    def on_btn_add(self, *args):
        pass

    def remove_item(self, item, *args):
        BaseBlock.remove_item(self, item, *args)
        if len(self.items) == 0:
            self.main.btn_process.configure(state='disabled')
            self.main.base_file_box.config(highlightbackground="red", highlightcolor="red", highlightthickness=1)

    def add_item(self, item_name, *args):
        BaseBlock.add_item(self, item_name, *args)
        self.main.btn_process.configure(state='active')
        if self.main.base_file_box:
            self.main.base_file_box.config(highlightthickness=0)
