"""
    All constant values are declared here.
"""
US_STATES = ['AK', 'AL', 'AR', 'AS', 'AZ', 'CA', 'CO', 'CT', 'DC', 'DE', 'FL', 'GA', 'GU', 'HI', 'IA', 'ID', 'IL',
             'IN', 'KS', 'KY', 'LA', 'MA', 'MD', 'ME', 'MI', 'MN', 'MO', 'MP', 'MS', 'MT', 'NA', 'NC', 'ND', 'NE',
             'NH', 'NJ', 'NM', 'NV', 'NY', 'OH', 'OK', 'OR', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VA',
             'VI', 'VT', 'WA', 'WI', 'WV', 'WY']

# TODO: Declare all city names here.
US_CITIES = [
    'Albuquerque, NM',
    'Atlanta, GA',
    'Austin, TX',
    'Baltimore, MD',
    'Boston, MA',
    'Charlotte, NC',
    'Chicago, IL',
    'Cleveland, OH',
    'Colorado Springs, CO',
    'Columbus, OH',
    'Dallas, TX',
    'Denver, CO',
    'Detroit, MI',
    'El Paso, TX',
    'Fort Worth, TX',
    'Fresno, CA',
    'Houston, TX',
    'Indianapolis, IN',
    'Jacksonville, FL',
    'Kansas City, MO',
    'Las Vegas, NV',
    'Long Beach, CA',
    'Los Angeles, CA',
    'Louisville, KY',
    'Memphis, TN',
    'Mesa, AZ',
    'Miami, FL',
    'Milwaukee, WI',
    'Minneapolis, MN',
    'Nashville, TN',
    'New Orleans, LA',
    'New York, NY',
    'Oakland, CA',
    'Oklahoma City, OK',
    'Omaha, NE',
    'Philadelphia, PA',
    'Phoenix, AZ',
    'Portland, OR',
    'Raleigh, NC',
    'Sacramento, CA',
    'San Antonio, TX',
    'San Diego, CA',
    'San Francisco, CA',
    'San Jose, CA',
    'Seattle, WA',
    'Tucson, AZ',
    'Tulsa, OK,'
    'Virginia Beach, VA',
    'Washington, DC',
    'Wichita, KS'
]

NUMBER_DICT = {
    'small': '0 ~ 100',
    'basic': '0 ~ 1000',
    'full': '0 ~ 9999',
    'years': '1930 ~ 2020'
}

SUBSTITUTION_CHECKS = ['a -> @', 'e -> 3', 'i -> 1', 'l -> 1', 'o -> 0', 's -> 5']

BIRTHDAY_FORMATS = {'mmddyy', 'ddmmyy', 'mmddyyyy', 'ddmmyyyy', 'mmyyyy', 'mmyy mmdd'}

SPECIAL_CHARACTERS = '!@#$%^&*()-=_+'

SPECIAL_TYPES = ['One at a time', 'All together']
