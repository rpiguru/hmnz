import Tkinter as Tk
from functools import partial
from widgets.base import BaseBlock
from widgets.const import SUBSTITUTION_CHECKS, SPECIAL_TYPES


class Mutator(BaseBlock):
    """
    Mutator Frame
    """

    def __init__(self, master=None, **kwargs):
        BaseBlock.__init__(self, master=master, title='Mutator', **kwargs)
        self.case_popup = None
        self.sp_case = None
        self.sub_popup = None
        self.chk_subs = []
        self.sub_type = None

    def add_upper_button(self):
        mb = Tk.Menubutton(self.upper_frame, text=" + ", relief="raised", font=("Helvetica", "14"))
        mb.menu = Tk.Menu(mb, tearoff=0)
        mb["menu"] = mb.menu
        mb.menu.add_command(label="Do Nothing", command=partial(self.add_item, 'Do Nothing'))

        m_cases = [None, None]
        for i, case in enumerate(['Lowercase', 'Uppercase']):
            m_cases[i] = Tk.Menu(mb, tearoff=0)
            mb.menu.add_cascade(label='{} ... '.format(case), menu=m_cases[i], underline=0)
            for _type in ['All', 'First']:
                label = "{} {}".format(case, _type)
                m_cases[i].add_command(label=label, command=partial(self.add_item, label))
            m_cases[i].add_command(label='{} Nth'.format(case), command=partial(self.open_case_popup, case))

        m_sub = Tk.Menu(mb, tearoff=0)
        mb.menu.add_cascade(label='Character Substitution ... ', menu=m_sub, underline=0)
        for _type in ['All', 'First', 'Last']:
            m_sub.add_command(label='Replace {}'.format(_type), command=partial(self.open_sub_popup, _type))

        mb.pack(side="left", fill="x", padx=10, pady=5)

    def on_btn_add(self, *args):
        pass

    def open_case_popup(self, _type):
        """
        Open Case Popup
        :param _type: `Lowercase` or `Uppercase`
        :return:
        """
        self.case_popup = Tk.Toplevel()
        self.case_popup.title('{}: Nth Character'.format(_type))
        self.case_popup.resizable(width=False, height=False)
        frame = Tk.Frame(self.case_popup)
        lb = Tk.Label(frame, text='Select Number of Nth Character'.format(self.title))
        lb.pack(fill='both', side='top')

        sp_box = Tk.Frame(frame)
        lb1 = Tk.Label(sp_box, text='Number: ')
        lb1.pack(side='left', padx=5)
        self.sp_case = Tk.Spinbox(sp_box, width=12, from_=1, to=10000)
        self.sp_case.pack(side='left')
        sp_box.pack(fill='both', side='top', padx=30, pady=20)

        btn_box = Tk.Frame(frame)
        btn_cancel = Tk.Button(btn_box, text='Cancel', command=self.cancel_case_popup)
        btn_cancel.pack(side='right', padx=10, pady=20)
        btn_ok = Tk.Button(btn_box, text='Ok', command=partial(self.on_ok_case_popup, _type))
        btn_ok.pack(side='left', padx=10, pady=20)
        btn_box.pack()
        frame.pack(fill='both', padx=10, pady=10)
        self.case_popup.focus_set()

    def cancel_case_popup(self, *args):
        if self.case_popup:
            self.case_popup.destroy()
            self.case_popup = None

    def on_ok_case_popup(self, _type, *args):
        """
        Callback when `OK` in Custom Number Window is selected.
        :param args:
        :return:
        """
        val_case = int(self.sp_case.get())
        ordinal = "%d%s" % (val_case, "tsnrhtdd"[(val_case / 10 % 10 != 1) * (val_case % 10 < 4) * val_case % 10::4])
        self.add_item('{}: {}'.format(_type, ordinal))
        self.cancel_case_popup()

    def open_sub_popup(self, _type):
        self.sub_popup = Tk.Toplevel()
        self.sub_popup.title('Replace {}'.format(_type))
        self.sub_popup.resizable(width=False, height=False)
        frame = Tk.Frame(self.sub_popup)
        lb = Tk.Label(frame, text='Select Substitution Checks'.format(self.title))
        lb.pack(fill='both', side='top')

        box = Tk.Frame(frame)
        self.chk_subs = []
        for val in SUBSTITUTION_CHECKS:
            var = Tk.IntVar()
            tmp = Tk.Checkbutton(box, text=val, relief=Tk.FLAT, variable=var)
            self.chk_subs.append(var)
            tmp.pack(fill='both', side='top')
        box.pack(fill='both', side='top', padx=30, pady=20)

        box_type = Tk.Frame(frame)
        self.sub_type = Tk.IntVar()
        for i, val in enumerate(SPECIAL_TYPES):
            tmp = Tk.Radiobutton(box_type, text=val, relief=Tk.FLAT, variable=self.sub_type, value=i)
            tmp.pack(fill='both', side='left')
        box_type.pack(fill='both', side='top', padx=30, pady=20)

        btn_box = Tk.Frame(frame)
        btn_cancel = Tk.Button(btn_box, text='Cancel', command=self.cancel_sub_popup)
        btn_cancel.pack(side='right', padx=10, pady=20)
        btn_ok = Tk.Button(btn_box, text='Ok', command=partial(self.on_ok_sub_popup, _type))
        btn_ok.pack(side='left', padx=10, pady=20)
        btn_box.pack()
        frame.pack(fill='both', padx=60, pady=10)
        self.sub_popup.focus_set()

    def cancel_sub_popup(self, *args):
        if self.sub_popup:
            self.sub_popup.destroy()
            self.sub_popup = None

    def on_ok_sub_popup(self, _type, *args):
        """
        Callback when `OK` in Custom Number Window is selected.
        :param args:
        :return:
        """
        checked_vals = [SUBSTITUTION_CHECKS[i] for i in range(len(SUBSTITUTION_CHECKS)) if self.chk_subs[i].get() == 1]
        if len(checked_vals) > 0:
            self.add_item('Replace {}: {} ({})'.format(_type, ', '.join(checked_vals),
                                                       SPECIAL_TYPES[self.sub_type.get()]))
        self.cancel_sub_popup()
